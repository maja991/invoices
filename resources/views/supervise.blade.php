@extends('layouts.layout')
@section('navigation')
    <li id="home"><a href="/supervise"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
    <li id="users"><a href="/users"><i class="glyphicon glyphicon-list"></i>Korisnici</a></li>
    <li id="log"><a href="/log"><i class="glyphicon glyphicon-time"></i> Logovanje </a></li>
    <li id="text"><a href="/text"><i class="glyphicon glyphicon-pencil"></i> Tekstovi </a></li>
    <li id="settings"><a href="/settings"><i class=" glyphicon glyphicon-cog"></i> Podesavanja </a></li>
    {{--<li id="template"><a href="/template"><i class="glyphicon glyphicon-pencil"></i> Template</a></li>--}}
    {{--<li id="invoice"><a href="/invoice"><i class="glyphicon glyphicon-record"></i> Fakture </a></li>--}}
@endsection
@section('content')
    <form method="get">
        Pregledajte fakture za period od <input type="text" class="datepicker" name="date_from"/> do <input type="text"
                                                                                                            class="datepicker"
                                                                                                            name="date_to"/>
        <input class='btn' type='submit' value='Pretrazi'/>
    </form>

    @if($users)
        <h3> Pregled faktura </h3>
        <table class='table table-striped'>
            <tr>
                <th class="text-center"> Korisnik</th>
                <th class="text-center"> Faktura</th>
            </tr>

            @foreach($users as $user)
                @if($user->invoices->count())
                    <tr>
                        <td class="text-center"> {{$user->name}}</td>
                        <td>
                            @foreach($user->invoices as $invoice)
                                <a class='btn btn-xs btn-info' href="/pdf/{{$invoice->id}}"
                                   target="_blank">PDF {{$invoice->id}} preview </a>
                            @endforeach
                        </td>
                    </tr>
                @else
                    <tr class="danger">
                        <td class="text-center"> {{$user->name}}</td>
                        <td> Nema!</td>
                    </tr>
                @endif
            @endforeach
        </table>

    @endif

@endsection

@section('js')
    <script>
        var element = document.getElementById("home");
        element.classList.add("current");
    </script>
@endsection