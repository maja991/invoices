@extends('layouts.layout')
@section('content')
    @if($errors->any())
        <h4 class="err">{{$errors->first()}}</h4>
    @endif
    <button class="btn btn-default" id="showFormClient">Unesi novog klijenta</button>
    <form action="{{action( 'ClientController@store' )}}" method="post" class="row" id="addClient">
    {{ csrf_field() }}
        <div class="form-group col-md-8 col-md-offset-2">
            <div class="row">
                <label class="col-xs-3 control-label">Ime kompanije/klijenta:</label>
                <div class="col-xs-9">
                    <input class='form-control' type="text" name='company_name'>
                </div>
            </div> <br>
            <div class="wrapClient" >
                <div class="row">
                    <div class="form-group" id="0">
                        <label class="col-xs-3 control-label">Dodatne informacije:</label>
                        <div class="col-xs-4">
                            <input class="form-control" name="informations[0][feature]" placeholder="Karakteristika (npr. Adresa, PIB...)"/>
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" name="informations[0][value]" placeholder="Informacija (npr. Svetog Save bb)" />
                        </div>
                        <div class="col-xs-1">
                            <button type="button" class="btn btn-default addButton1"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <input class='btn btn-success' type="submit" name="submit" value="Submit">
        </div>
    </form>
    <h3> Pregled klijenata</h3>

    <table class='table table-striped'>
        <tr>
            <th class="text-center">Ime kompanije/klijenta</th>
            <th colspan='2' class='text-center'> Akcija</th>
        </tr>
        @foreach($clients as $client)
                <tr class='form-group'>
                    <td>{{$client->company_name}}</td>
                    <td><a type="button" class="btn btn-md btn-success" href="/client/{{$client->id}}/edit" >
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Show / Edit
                        </a></td>
                    <form action="{{action('ClientController@destroy')}}" method="post" >
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <td class="empty"><input class='form-control' type='hidden' name='id' value='{{$client->id}}'></td>
                        <td><button type="submit" class="btn btn-danger btn-md" name="delete" onclick="return confirm('Da li ste sigurni da želite da obrisete ovog klijenta?');">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>Delete
                        </button></td>
                    </form>
                </tr>
            @endforeach

    </table>
@endsection
@section('js')
    <script>
        var element = document.getElementById("client");
        element.classList.add("current");

        $(document).ready(function(){
            $("#showFormClient").click(function(){
                $("#addClient").toggle();
            });
        });

        var x = 1;

    </script>
@endsection