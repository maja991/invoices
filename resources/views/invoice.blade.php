@extends('layouts.layout')

@section('content')
    @if($errors->any())
        <h4 class="err">{{$errors->first()}}</h4>
    @endif
    <button class="btn btn-default" id="showForm">Kreiraj novu fakturu</button>
    <form action="{{action( 'InvoiceController@store' )}}" method="post" class="form-horizontal" id="invoiceForm">
        {{ csrf_field() }}
        <div class="form-group row">
            <div class="col-md-6">
                <div class="form-inline">
                    <label>Broj fakture/godina</label>
                    <input class='form-control' type="text" name='invoice_num' size='6' value="{{$invoiceNumber}}">/
                    <input class='form-control' type="text" name='year' size='4' value="{{$year}}">
                </div>
                <label>Datum prometa usluga</label>
                <input class='form-control datepicker' type="text" name='traffic_date' value="{{$traffic_date}}">
                <label>Datum izdavanja računa</label>
                <input class='form-control datepicker' type="text" name='publishing_date' value="{{$publishing_date}}">
                <label>Datum valute</label>
                <input class='form-control datepicker' type="text" name='currency_date' value="{{$currency_date}}">
            </div>
            <div class="col-md-6">
                <label>Mesto</label>
                <input class='form-control' type="text" name='location' value="{{$location}}">
                <label>Odaberi željeni templejt:</label>
                <select class='form-control' name="template">
                    @foreach ($templates as $template)
                        <option value='{{$template->id}}'>{{$template->company_name}}</option>
                    @endforeach
                </select>
                <label>Odaberi klijenta:</label>
                <select class='form-control' name="client">
                    @foreach ($clients as $client)
                        <option value='{{$client->id}}'>{{$client->company_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="wrap">
            <div class="row">
                <div class="form-group" id="0">
                    <label class="col-xs-1 control-label">Stavke za naplatu:</label>
                    <div class="col-xs-7">
                        <textarea class="form-control" name="items[0][description]"
                                  placeholder="Opis">{{$item["description"]}}</textarea>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" class="form-control" name="items[0][price]" placeholder="Cena"
                               value="{{$item['price']}}"/>
                    </div>
                    <div class="col-xs-1">
                        <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <input class='btn btn-success' type="submit" name="submit" value="Submit">
    </form>

    <h3> Pregled faktura</h3>
    <table class='table table-striped'>
        <tr>
            <th class="text-center">Broj fakture</th>
            <th class="text-center">Klijent</th>
            <th class="text-center">Iznos</th>
            <th colspan='3' class='text-center'> Akcija</th>
        </tr>

        @foreach($invoices as $invoice)
            <tr>
                <td>{{$invoice->invoice_num}}</td>
                <td>{{$invoice->client->company_name}}</td>
                <td>{{number_format($invoice->amount, 0, ',', ' ')}}</td>
                <td><a class='btn btn-md btn-info' href="/pdf/{{$invoice->id}}" target="_blank">PDF preview</a></td>
                <td><a type="button" class="btn btn-md btn-success" href="/invoice/{{$invoice->id}}/edit">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Show / Edit
                    </a></td>
                <form action="{{action( 'InvoiceController@destroy' )}}" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <td class="empty"><input class='form-control' type='hidden' name='id' value='{{$invoice->id}}'></td>
                    <td>
                        <button type="submit" class="btn btn-danger btn-md" name="delete"
                                onclick="return confirm('Da li ste sigurni da zelite da obrisete ovu fakturu?');">
                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                        </button>
                    </td>
                </form>
            </tr>
        @endforeach
    </table>

@endsection

@section('js')
    <script>
        var element = document.getElementById("invoice");
        element.classList.add("current");

        $(document).ready(function () {
            $("#showForm").click(function () {
                $("#invoiceForm").toggle();
            });
        });

        var x = 1;
    </script>
@endsection