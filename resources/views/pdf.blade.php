<!DOCTYPE html>
<html>
<head>
    <title>Generator faktura</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/invoice.css">
</head>
<body>
<div class="container">
    <header class="row" id="from-to">
        <div class="col-xs-6">
            <table class="table-condensed text-left" id="from">
                <tr><th colspan="2" style="line-height: 110%">{{$template->company_name}}</th></tr>
                @if($n>0)
                    @foreach($informationsTem as $info)
                        <tr style="width: 100%;"><td class="narrow" style="width: 30%;">{{$info["feature"]}}:</td><td >{{$info["value"]}}</td></tr>
                    @endforeach
                @endif
            </table>
        </div>
        <div class="col-xs-6">
            <table class="table-condensed text-left" id="to">
                <tr><th colspan="2">{{$client->company_name}}</th></tr>
                @if($m>0)
                    @foreach($informationsCli as $info)
                        <tr style="width: 100%;"><td class="narrow" style="width: 30%;">{{$info["feature"]}}:</td><td>{{$info["value"]}}</td></tr>
                    @endforeach
                @endif
            </table>
        </div>
    </header>
    <div>
        <h3 class="text-center"><b>FAKTURA {{$invoice->invoice_num}}/{{$invoice->year}}</b></h3>

        <div class="row" id="table">
            <div class="col-xs-5 col-xs-offset-6">
                <table class="table-condensed text-left">
                    <tr><td>Datum prometa usluga:</td><td>{{$invoice->traffic_date}}</td></tr>
                    <tr><td>Datum izdavanja računa:</td><td>{{$invoice->publishing_date}}</td></tr>
                    <tr><td>Mesto izdavanja računa:</td><td>{{$invoice->location}}</td></tr>
                    <tr><td>Datum valute:</td><td>{{$invoice->currency_date}}</td></tr>
                </table>
            </div>
        </div>
        <table class="text-center" id="items">
            <thead class="{{$template->table_header}}">
            <tr>
                <td colspan="2" class="text-center" width="75%" style="{{$tableBorder}}">Opis</td>
                <td class="text-center" width="25%" style="{{$tableBorder}}">Neto</td>
            </tr>
            </thead>
            @foreach($items as $item)
                <tr>
                    <td colspan="2" style="{{$tableBorder}}{{$tablePadding}}">{{$item["description"]}}</td>
                    <td style="{{$tableBorder}}">{{number_format($item["price"], 2, '.', ',')}} Din.</td>
                </tr>
            @endforeach
            <tr><td width="50%"></td><th width="50%" style="{{$tableBorder}}">Ukupno za uplatu:</th><td style="{{$tableBorder}}">{{number_format($invoice->amount, 2, '.', ',')}} Din.</td></tr>
        </table>
    </div>
    <footer>
        <div id="additional">@php echo nl2br($template->additional_text) @endphp</div>
    </footer>
</div>
</body>
</html>