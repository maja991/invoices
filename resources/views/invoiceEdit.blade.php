@extends('layouts.layout')

@section('content')

    @if( isset($message))
        <h4>{{$message}}</h4>
    @endif
    <h3> Pregled fakture br.{{$invoice->invoice_num}}/{{$invoice->year}}</h3>
    <form action="{{--{{action( 'InvoiceController@update' )}}--}}" method="post" class="form-horizontal" >
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group row">
            <div class="col-md-6">
                <div class="form-inline">
                    <label>Broj fakture/godina</label>
                    <input class='form-control' type="text" name='invoice_num' size='6' value="{{$invoice->invoice_num}}">/
                    <input class='form-control' type="text" name='year' size='4' value="{{$invoice->year}}">
                </div>
                <label>Datum prometa usluga</label>
                <input class='form-control datepicker' type="text" name='traffic_date' value="{{$invoice->traffic_date}}">
                <label>Datum izdavanja računa</label>
                <input class='form-control datepicker' type="text" name='publishing_date' value="{{$invoice->publishing_date}}">
                <label>Datum valute</label>
                <input class='form-control datepicker' type="text" name='currency_date' value="{{$invoice->currency_date}}">
            </div>
            <div class="col-md-6">
                <label>Mesto</label>
                <input class='form-control' type="text" name='location' value="{{$invoice->location}}">
                <label>Odaberi željeni templejt:</label>
                <select class='form-control' name="template" id="invoiceTemplate">
                    @foreach ($templates as $template)
                        <option value='{{$template->id}}'>{{$template->company_name}}</option>
                    @endforeach
                </select>
                <label>Odaberi klijenta:</label>
                <select class='form-control' name="client" id="invoiceClient">
                    @foreach ($clients as $client)
                        <option value='{{$client->id}}'>{{$client->company_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="wrap" >
            <div class="row">
                <div class="form-group" id="0">
                    <label class="col-xs-1 control-label">Stavke za naplatu:</label>
                    <div class="col-xs-7">
                        <textarea class="form-control" name="items[0][description]">{{$items[0]['description']}}</textarea>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" class="form-control" name="items[0][price]" value="{{$items[0]['price']}}" />
                    </div>
                    <div class="col-xs-1">
                        <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>
            @if($m>1)
                @for($i=1;$i<$m;$i++)
                    <div class='row'><div class='form-group' id='{{$i}}'>
                        <label class='col-xs-1 control-label'></label>
                        <div class='col-xs-7'>
                            <textarea class='form-control' name='items[{{$i}}][description]'>{{$items[$i]['description']}}</textarea>
                        </div>
                        <div class='col-xs-3'>
                            <input type='number' class='form-control' name='items[{{$i}}][price]' value="{{$items[$i]['price']}}"/>
                        </div>
                        <div class='col-xs-1'>
                           <button type='button' class='btn btn-default remove_field'><i class='fa fa-minus'></i></button>
                        </div>
                         </div></div>
                @endfor
            @endif
        </div>
        <input class='btn btn-success' type="submit" name="submit" value="Submit">
    </form>

@endsection

@section('js')
    <script>
        var element = document.getElementById("invoice");
        element.classList.add("current");

        $("#invoiceTemplate").val( {{$invoice->template->id}} );
        $("#invoiceClient").val( {{$invoice->client->id}} );


        var x = {{$m}};

    </script>
@endsection