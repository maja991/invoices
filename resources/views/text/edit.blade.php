@extends('layouts.layout')

@section('navigation')
    <li id="home"><a href="/supervise"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
    <li id="users"><a href="/users"><i class="glyphicon glyphicon-list"></i>Korisnici</a></li>
    <li id="log"><a href="/log"><i class="glyphicon glyphicon-time"></i> Logovanje </a></li>
    <li id="text"><a href="/text"><i class="glyphicon glyphicon-pencil"></i> Tekstovi </a></li>
    <li id="settings"><a href="/settings"><i class=" glyphicon glyphicon-cog"></i> Podesavanja </a></li>
@endsection

@section('content')

    <h3> Tekstovi</h3>

    @if ($message=Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
    @endif

    @if ($message=Session::get('error'))
        <div class="alert alert-danger">
            <p>{{$message}}</p>
        </div>
    @endif

    <form action="{{ url('update')}}" method="GET">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <strong>Tekst 1:</strong>
                    <textarea name="text1" class="form-control"> {{(isset($text->text1))?$text->text1:null}}</textarea>
                    <strong>Tekst 2:</strong>
                    <textarea name="text2" class="form-control"> {{(isset($text->text2))?$text->text2:null}}</textarea>
                    <strong>Tekst 3:</strong>
                    <textarea name="text3" class="form-control"> {{(isset($text->text3))?$text->text3:null}}</textarea>
                    <strong>Tekst 4:</strong>
                    <textarea name="text4" class="form-control"> {{(isset($text->text4))?$text->text4:null}}</textarea>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-left"><br>
            <button type="submit" class="btn btn-primary">Sacuvaj</button>
        </div>
    </form>
@endsection

@section('js')
    <script>
        var element = document.getElementById("text");
        element.classList.add("current");
    </script>
@endsection