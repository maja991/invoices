@extends('layouts.layout')
@section('content')
    @if($errors->any())
        <h4 class="err">{{$errors->first()}}</h4>
    @endif

    <button class="btn btn-default" id="showForm">Kreirajte novi templejt</button>

    <form action="{{action( 'TemplateController@store' )}}" method="post" id="addTemplate" class="row">
        {{ csrf_field() }}
        <div class="form-group col-md-8 col-md-offset-2">
            <div class="row">
                <label class="col-xs-3 control-label">Vaše ime ili ime kompanije:</label>
                <div class="col-xs-9">
                    <input class='form-control' type="text" name='company_name'>
                </div>
            </div>
            <div class="row">
                <label class="col-xs-3 control-label">Odaberi stil tabele:</label>
                <div class="col-xs-9">
                    <select class="form-contro" name="styles_id">
                        @foreach($styles as $style)
                            <option value="{{$style->id}}">{{$style->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-xs-3 control-label">Dodatni tekst na kraju fakture:</label>
                <div class="col-xs-9">
                    <textarea class='form-control' name='additional_text'></textarea>
                </div>
            </div>

            <div class="wrapTemplate">
                <div class="row">
                    <div class="form-group" id="0">
                        <label class="col-xs-3 control-label">Dodatne informacije:</label>
                        <div class="col-xs-4">
                            <input class="form-control" name="informations[0][feature]"
                                   placeholder="Karakteristika (npr. Adresa, PIB...)"/>
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" name="informations[0][value]"
                                   placeholder="Informacija (npr. Svetog Save bb)"/>
                        </div>
                        <div class="col-xs-1">
                            <button type="button" class="btn btn-default addButtonTem"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <input class='btn btn-success' type="submit" name="submit" value="Submit">
        </div>
    </form>

    <h3> Pregled templejta</h3>

    <table class='table table-striped'>
        <tr>
            <th class="text-center">Ime Vase kompanije</th>
            <th colspan='2' class='text-center'> Akcija</th>
        </tr>

        @foreach($templates as $template)
            <tr>
                <td>{{$template->company_name}}</td>
                <td><a type="button" class="btn btn-md btn-success" href="/template/{{$template->id}}/edit">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Show / Edit
                    </a></td>
                <form action="{{action( 'TemplateController@destroy' )}}" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <td class="empty"><input class='form-control' type='hidden' name='id' value='{{$template->id}}'>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-danger btn-md" name="delete"
                                onclick="return confirm('Da li ste sigurni da želite da obrisete ovaj templejt?');">
                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                        </button>
                    </td>
                </form>
            </tr>
        @endforeach
    </table>


@endsection

@section('js')
    <script>
        var element = document.getElementById("template");
        element.classList.add("current");

        $(document).ready(function () {
            $("#showForm").click(function () {
                $("#addTemplate").toggle();
            });
        });

        var x = 1;

    </script>
@endsection