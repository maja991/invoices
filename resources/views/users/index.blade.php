@extends('layouts.layout')
@section('navigation')
    <li id="home"><a href="/supervise"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
    <li id="users"><a href="/users"><i class="glyphicon glyphicon-list"></i>Korisnici</a></li>
    <li id="log"><a href="/log"><i class="glyphicon glyphicon-time"></i> Logovanje </a></li>
    <li id="text"><a href="/text"><i class="glyphicon glyphicon-pencil"></i> Tekstovi </a></li>
    <li id="settings"><a href="/settings"><i class=" glyphicon glyphicon-cog"></i> Podesavanja </a></li>
@endsection
@section('content')

    <h3>Pregled korisnika</h3>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('users.create') }}">Kreiraj novog korisnika</a>
            </div>
        </div>
    </div>
    <br>

    @if ($message=Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
    @endif

    @if ($message=Session::get('error'))
        <div class="alert alert-danger">
            <p>{{$message}}</p>
        </div>
    @endif

    <table class='table table-striped'>
        <tr>
            <th class="text-center"> Korisnik</th>
            <th class="text-center"> Status</th>
            <th class="text-center"> email</th>
            <th class="text-center"> Tip posla</th>
            <th class="text-center"> Projekat</th>
            <th class="text-center"> Opcije</th>
        </tr>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{ $user->active?"Aktivan":"Nije aktivan"  }}</td>
                <td>{{$user->email}}</td>

                <td>
                    @if($user->jobTypes)
                        {{$user->jobTypes->name}}
                    @else /
                    @endif
                </td>
                <td>
                    @if($user->project)
                        {{$user->project}}
                    @else /
                    @endif
                </td>
                <td>
                    <a class="btn btn-primary" href="{{ route('users.edit', $user->id) }}">Izmeni</a>

                    <form action="{{route('users.destroy', $user->id)}}" method="POST" style="display: inline-block">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <a class="btn btn-danger">Izbrisi</a>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

@endsection

@section('js')
    <script>
        var element = document.getElementById("users");
        element.classList.add("current");
    </script>
@endsection
