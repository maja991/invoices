@extends('layouts.layout')

@section('navigation')
    <li id="home"><a href="/supervise"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
    <li id="users"><a href="/users"><i class="glyphicon glyphicon-list"></i>Korisnici</a></li>
    <li id="log"><a href="/log"><i class="glyphicon glyphicon-time"></i> Logovanje </a></li>
    <li id="text"><a href="/text"><i class="glyphicon glyphicon-pencil"></i> Tekstovi </a></li>
    <li id="settings"><a href="/settings"><i class=" glyphicon glyphicon-cog"></i> Podesavanja </a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}">Nazad</a>
            </div>
        </div>
    </div>

    @if (count($errors) >0)
        <div class="alert alert-danger">
            <strong>Greska!</strong> Problem sa unosom.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('users.store') }}" method="POST">
        {{ csrf_field() }}
        @include('users.form')
    </form>

@endsection

@section('js')
    <script>
        var element = document.getElementById("users");
        element.classList.add("current");
    </script>
@endsection

