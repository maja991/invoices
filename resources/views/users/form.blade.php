<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <strong>Ime i prezime:</strong>
            <input type="text" name="name" value="{{(isset($user))?$user->name:null}}" placeholder="ime i prezime"
                   class="form-control">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <strong>email:</strong><br>
            <input type="text" name="email" value="{{(isset($user))?$user->email:'@quantox.com'}}" class="form-control">

        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <strong>Tip posla:</strong><br>
            @if(isset($user))
                <select name="job_id">
                    @foreach($jobs as $job)
                        <option value="{{$job->id}}"
                                @if($user->job_id==$job->id) selected="selected" @endif>{{$job->name}}</option>
                    @endforeach
                </select>
            @else
                <select name="job_id">
                    @foreach($jobs as $job)
                        <option value="{{$job->id}}">{{$job->name}}</option>
                    @endforeach
                </select>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <strong>Projekat:</strong><br>
            <input type="text" name="project" value="{{(isset($user))?$user->project:null}}" placeholder="projekat"
                   class="form-control">
        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 text-left"><br>
    <button type="submit" class="btn btn-primary">Sacuvaj</button>
</div>