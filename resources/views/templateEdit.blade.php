@extends('layouts.layout')
@section('content')
    @if( isset($message))
        <h4>{{$message}}</h4>
    @endif
    <h3> Pregled i/ili izmena templejta {{$template->company_name}}</h3>
    <form action="{{--{{action( 'TemplateController@update )}}--}}" method="post"  class="row">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group col-md-8 col-md-offset-2">
            <div class="row">
                <label class="col-xs-3 control-label">Vaše ime ili ime kompanije:</label>
                <div class="col-xs-9">
                    <input class='form-control' type="text" name='company_name' value='{{$template->company_name}}'>
                </div>
            </div>
            <div class="row">
                <label class="col-xs-3 control-label">Odaberi stil tabele:</label>
                <div class="col-xs-9">
                    <select class="form-contro" name="styles_id" id="templateStyle">
                        @foreach($styles as $style)
                            <option value="{{$style->id}}">{{$style->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-xs-3 control-label">Dodatni tekst na kraju fakture:</label>
                <div class="col-xs-9">
                    <textarea class='form-control' name='additional_text'>{{$template->additional_text}}</textarea>
                </div>
            </div>

            <div class="wrapTemplate" >
                <div class="row">
                    <div class="form-group" id="0">
                        <label class="col-xs-3 control-label">Dodatne informacije:</label>
                        <div class="col-xs-4">
                            <input class="form-control" name="informations[0][feature]" placeholder="Karakteristika (npr. Adresa, PIB...)" value="{{$informations[0]['feature']}}" />
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" name="informations[0][value]" placeholder="Informacija (npr. Svetog Save bb)" value="{{$informations[0]['value']}}"/>
                        </div>
                        <div class="col-xs-1">
                            <button type="button" class="btn btn-default addButtonTem"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                @if($n>0)
                    @for($i=1;$i<$n;$i++)
                        <div class='row'><div class='form-group' id='{{$i}}'>
                                <label class='col-xs-3 control-label'></label>
                                <div class='col-xs-4'>
                                    <input type='text' class='form-control' name='informations[{{$i}}][feature]' value="{{$informations[$i]['feature']}}"/>
                                </div>
                                <div class='col-xs-4'>
                                    <input type='text' class='form-control' name='informations[{{$i}}][value]' value="{{$informations[$i]['value']}}"/>
                                </div>
                                <div class='col-xs-1'>
                                    <button type='button' class='btn btn-default remove_field1'><i class='fa fa-minus'></i></button>
                                </div>
                            </div></div>
                    @endfor
                @endif
            </div>
            <input class='btn btn-success' type="submit" name="submit" value="Submit">
        </div>
    </form>


@endsection

@section('js')
    <script>
        var element = document.getElementById("template");
        element.classList.add("current");

        $(document).ready(function(){
            $("#showForm").click(function(){
                $("#addTemplate").toggle();
            });
        });

        $("#templateStyle").val( {{$template->styles_id}} );

        var x = {{$n}};

    </script>
@endsection