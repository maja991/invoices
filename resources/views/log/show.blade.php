@extends('layouts.layout')

@section('navigation')
    <li id="home"><a href="/supervise"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
    <li id="users"><a href="/users"><i class="glyphicon glyphicon-list"></i>Korisnici</a></li>
    <li id="log"><a href="/log"><i class="glyphicon glyphicon-time"></i> Logovanje </a></li>
    <li id="text"><a href="/text"><i class="glyphicon glyphicon-pencil"></i> Tekstovi </a></li>
    <li id="settings"><a href="/settings"><i class=" glyphicon glyphicon-cog"></i> Podesavanja </a></li>
@endsection

@section('content')

    <h3>Pregled logovanja</h3>

    @if($logs->isNotEmpty())

        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <label for="date">Mesec:</label>
                <div class="form-inline">
                    <input type="text" name="date" class="form-control date-picker" id="date">
                    <button type="button" class="btn btn-info" id="search">Pretrazi</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" id="excel">Generisi timesheet</button>
                </div>
            </div>
        </div>
        <br>

        <div id="month">
            <table class='table table-striped'>
                <tr>
                    <th class="text-center"> Ime i prezime</th>
                    <th class="text-center"> Projekat</th>
                    <th class="text-center"> Datum</th>
                    <th class="text-center"> Vreme</th>
                    <th class="text-center"> Opis</th>
                </tr>

                @foreach($logs as $log)
                    <tr>
                        <td>{{$log->users->name}}</td>
                        <td>{{$log->users->project}}</td>
                        <td>{{$log->date}}</td>
                        <td>{{($hour = floor($log->time/60))?$hour .'h':''}} {{($min = $log->time%60)?$min . 'min': ''}}</td>
                        <td>{{$log->description}}</td>
                    </tr>
                    <input type="hidden" id='user_id' value="{{$log->users->id}}">
                @endforeach
            </table>
        </div>
    @else
        Logs don't exist.
    @endif
@endsection

@section('js')
    @if($logs->isNotEmpty())
        <script type="text/javascript">
            $(function () {
                $('.date-picker').datepicker({
                    changeMonth: true,
                    dateFormat: 'mm',
                    onClose: function (dateText, inst) {
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                });
            });
            $("button#search").click(function () {
                var date = $('#date').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "/month",
                    type: "get",
                    data: {data: date, user_id: '{{$log->users->id}}'},
                    success: function (response) {
                        $('#month').html(response);
                    }
                });
            });

            $("button#excel").click(function () {
                var date = $('#date').val();
                var user = '{{$log->users->id}}';
                if
                (date == '') alert('Izaberi mesec');
                else
                    window.open('/downloadExcel?user_id=' + user + '&date=' + date, '_blank');
            });

            var element = document.getElementById("log");
            element.classList.add("current");

        </script>
    @endif
@endsection
