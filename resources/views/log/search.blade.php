<table class='table table-striped'>
    <tr>
        <th class="text-center"> Ime i prezime</th>
    </tr>

    @foreach($users as $user)
        <tr>
            <td><a href="{{route('log.show', $user->id)}}"> {{$user->name}}</a></td>

        </tr>
    @endforeach
</table>