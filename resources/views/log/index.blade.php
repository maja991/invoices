@extends('layouts.layout')

@section('navigation')
    <li id="home"><a href="/supervise"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
    <li id="users"><a href="/users"><i class="glyphicon glyphicon-list"></i>Korisnici</a></li>
    <li id="log"><a href="/log"><i class="glyphicon glyphicon-time"></i> Logovanje </a></li>
    <li id="text"><a href="/text"><i class="glyphicon glyphicon-pencil"></i> Tekstovi </a></li>
    <li id="settings"><a href="/settings"><i class=" glyphicon glyphicon-cog"></i> Podesavanja </a></li>

@endsection

@section('content')

    <h3>Svi korisnici</h3>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <strong>Pretrazi korisnike:</strong>
            <div class="form-inline">
                <input type="text" name="name" class="form-control" id="user_name">
                <button type="button" class="btn btn-info" id="search">Pretrazi</button>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-left">

        <br><br>
    </div>

    <div id="user">

        <table class='table table-striped'>
            <tr>
                <th class="text-center"> Ime i prezime</th>
            </tr>

            @foreach($users as $user)
                <tr>
                    <td><a href="{{route('log.show', $user->id)}}"> {{$user->name}}</a></td>

                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $("button#search").click(function () {
            var name = $('#user_name').val();
            console.log(name);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "./search",
                type: "get",
                data: {data_name: name},
                success: function (response) {
                    $('#user').html(response);
                }
            });
        });

        var element = document.getElementById("log");
        element.classList.add("current");
    </script>
@endsection

