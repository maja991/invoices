<table class='table table-striped'>
    <tr>
        <th class="text-center"> Ime i prezime</th>
        <th class="text-center"> Projekat</th>
        <th class="text-center"> Datum</th>
        <th class="text-center"> Vreme</th>
        <th class="text-center"> Opis</th>
    </tr>
    @foreach($logs as $log)
        <tr>
            <td>{{$log->users->name}}</td>
            <td>{{$log->users->project}}</td>
            <td>{{$log->date}}</td>
            <td>{{($hour = floor($log->time/60))?$hour .'h':''}} {{($min = $log->time%60)?$min . 'min': ''}}</td>
            <td>{{$log->description}}</td>
        </tr>
    @endforeach
</table>