@extends('layouts.layout')

@section('navigation')
    <li id="home"><a href="/supervise"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
    <li id="users"><a href="/users"><i class="glyphicon glyphicon-list"></i>Korisnici</a></li>
    <li id="log"><a href="/log"><i class="glyphicon glyphicon-time"></i> Logovanje </a></li>
    <li id="text"><a href="/text"><i class="glyphicon glyphicon-pencil"></i> Tekstovi </a></li>
    <li id="settings"><a href="/settings"><i class=" glyphicon glyphicon-cog"></i> Podesavanja </a></li>

@endsection

@section('content')

    <h3>Loguj vreme:</h3>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <label for="date">Izaberi mesec:</label>
            <div class="form-inline">
                <input type="text" name="date" class="form-control date-picker" id="date">
                <button type="button" class="btn btn-info" id="search">Loguj</button>
            </div>
        </div>
    </div><br>

    <div id="loading" style="display: none">
        <img src="/images/loading.gif" alt="loading" style="left: auto" height="20%" width="20%"/>
        <h5>Podaci se upisuju...</h5>
    </div>

@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            $('.date-picker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm',
                onClose: function (dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
        });

        $("button#search").click(function () {
            var date = $('#date').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#loading").show();
            $.ajax({
                url: "/logs",
                type: "get",
                data: {data: date},
                success: function () {
                    $("#loading").hide();
                    alert("Uspesno je logovano vreme za izabrani mesec!");
                },
                error: function () {
                    alert("Greska");
                }
            });
        });

        var element = document.getElementById("settings");
        element.classList.add("current");
    </script>
@endsection
