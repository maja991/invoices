<!DOCTYPE html>
<html>
<head>
	<title>Generator faktura</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- styles -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- Bootstrap -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

	<link href="/css/styles.css" rel="stylesheet">

	<link rel="stylesheet" href="{{asset('/css/jquery-ui.css')}}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

</head>
<body>
<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<!-- Logo -->
				<div class="logo">
					<h1><a href="/home">Aplikacija za kreiranje faktura</a></h1>
				</div>
			</div>
			<div class="col-md-2 col-md-offset-5">
				<div class="navbar navbar-inverse" role="banner">
					<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									{{ Auth::user()->name }} <span class="caret"></span>
								</a>

								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="{{ url('/logout') }}"
										   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
											Logout
										</a>

										<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="page-content">
	<div class="row">
		<div class="col-md-2">
			<div class="sidebar content-box" style="display: block;">
				<ul class="nav">
					@section('navigation')
					<!-- Main menu -->
					<li id="home" ><a href="/home"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
					<li id="client"><a href="/client"><i class="glyphicon glyphicon-list"></i> Klijenti</a></li>
					<li id="template"><a href="/template"><i class="glyphicon glyphicon-pencil"></i> Template</a></li>
					<li id="invoice"><a href="/invoice"><i class="glyphicon glyphicon-record"></i> Fakture </a></li>
					@show
				</ul>
			</div>
		</div>
		<div class="col-md-10">
			@yield('content')
		</div>
	</div>
</div>
<footer>
	<div class="container">

		<div class="copy text-center">
			Copyright 2014 <a href='#'>Website</a>
		</div>

	</div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/js/custom.js"></script>
<script src="{{asset('/js/jquery-ui.js')}}"></script>
<script type="text/javascript">
    $(function () {
        $(document).ready(function(){
            $('.datepicker').datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });
</script>

@yield('js')

</body>
</html>