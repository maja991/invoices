@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="content-box-large">
            <div class="panel-heading">
                <div class="panel-title">Pregled Vaših faktura</div>
            </div>
            <div class="panel-body">
                <table class='table table-striped'>
                    <tr>
                        <th class='text-center'>Broj</th>
                        <th class='text-center'>Klijent</th>
                        <th class='text-center'>Iznos</th>
                        <th class='text-center'> Akcija</th>
                    </tr>
                    @foreach ($invoices as $invoice)
                        <tr class='text-center' >
                            <td>{{$invoice->invoice_num}}</td>
                            <td>{{$invoice->client->company_name}}</td>
                            <td>{{number_format($invoice->amount, 0, ',', ' ')}}</td>
                            <td><a class='btn btn-xs btn-info' href="/pdf/{{$invoice->id}}" target="_blank">Preview</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title">Pregled Vaših klijenata</div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class='table table-striped'>
                        <tr>
                            <th class='text-center'>Ime</th>
                            <th class='text-center'> Akcija</th>
                        </tr>
                        @foreach ($clients as $client)
                            <tr class='text-center' >
                                <td>{{$client->company_name}}</td>
                                <td><button  data-name="{{$client->company_name}}" data-info="{{$client->informations}}" class="client btn btn-info btn-xs" data-target="#clientModal">Info</button></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-box-header">
                    <div class="panel-title">Pregled Vaših templejta</div>
                </div>
                <div class="content-box-large box-with-header">
                    <table class='table table-striped'>
                        <tr>
                            <th class='text-center'>Ime</th>
                            <th class='text-center'> Akcija</th>
                        </tr>
                        @foreach ($templates as $template)
                            <tr class='text-center' >
                                <td>{{$template->company_name}}</td>
                                <td><button  data-all="{{$template}}" data-info="{{$template->informations}}" data-style="{{App\Styles::find($template->styles_id)->name}}" class="template btn btn-info btn-xs" data-target="#templateModal">Info</button></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal Template-->
        <div class="modal fade" id="templateModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Informacije o templejtu <span id="company_name_tem"></span></h4>
                    </div>
                    <div class="modal-body" id="modal-tem">
                        <table id="templateTable" class="table"></table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal Client-->
        <div class="modal fade" id="clientModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Informacije o klijentu <span id="company_name_cli"></span></h4>
                    </div>
                    <div class="modal-body" id="modal-cli">
                        <table id="clientTable" class="table"></table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    var element = document.getElementById("home");
    element.classList.add("current");

    $(document).ready(function(){
        $(".template").click(function(){
            var template  = $(this).data('all');
            var info      = $(this).data('info');
            var style     = $(this).data('style');
            $(".appended").remove();
            $("#company_name_tem").append("<b class='appended'>"+ template.company_name + "</b>");
            $("#templateTable").append("<tr class='appended'><th>Stil</th><td>"+ style +"</td></tr>");
            if (info != "") {
                $.each(info, function (index, value) {
                    $("#templateTable").append("<tr class='appended'><th>"+ value.feature+"</th><td>"+ value.value+"</td></tr>");
                });
            };
            $("#templateTable").append("<tr class='appended'><th>Dodatni tekst</th><td>"+ template.additional_text+"</td></tr>");
            $("#templateModal").modal();
        });
    });


</script>
@endsection