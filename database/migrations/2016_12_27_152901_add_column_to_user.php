<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('invoice_num')->default(0);
            $table->integer('year')->default(2016);
            $table->string('place')->default('Cacak');
            $table->string('item')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('invoice_num');
            $table->dropColumn('year');
            $table->dropColumn('place');
            $table->dropColumn('item');
        });
    }
}
