<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_id')->unsigned();
            $table->foreign('template_id')
                ->references('id')->on('templates')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->longText('items');
            $table->string('location');
            $table->string('traffic_date');
            $table->string('publishing_date');
            $table->string('currency_date');
            $table->integer('amount');
            $table->integer('invoice_num');
            $table->integer('year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
