<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function ($table) {

            $table->timestamps();
            $table->date('traffic_date')->change();
            $table->date('publishing_date')->change();
            $table->date('currency_date')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function($table) {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->string('traffic_date')->change();
            $table->string('publishing_date')->change();
            $table->string('currency_date')->change();
        });
    }
}
