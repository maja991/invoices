<?php

use Illuminate\Database\Seeder;
use App\JobType;

class JobTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobNames = ['undefined', 'BE', 'FE', 'Design', 'PM', 'QA', 'Android', 'iOS', 'Office Assistant', 'HR assistant', 'HR manager'];

        foreach ($jobNames as $jobName) {
            $jobType = new JobType;
            $jobType->name = $jobName;
            $jobType->save();
        }
    }
}
