<?php

use Illuminate\Database\Seeder;


class StyleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('styles')->insert([
            'id'   => 1,
            'name' => 'plain',
        ]);

        DB::table('styles')->insert([
            'id'   => 2,
            'name' => 'compact',
        ]);
    }
}
