<?php

use Illuminate\Database\Seeder;

class StylesFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('style_fields')->insert([
                'id'         => 1,
                'style_id'   => 1,
                'properties' => 'tablePadding',
                'value'      => 'padding: 50px 20px;',
            ]);
        DB::table('style_fields')->insert([
                'id'         => 2,
                'style_id'   => 1,
                'properties' => 'tableBorder',
                'value'      => 'border-style: solid; border-width:1px;border-color: black;',
            ]);
        DB::table('style_fields')->insert([
                'id'         => 3,
                'style_id'   => 2,
                'properties' => 'tableBorder',
                'value'      => 'border-style: solid; border-width:1px;border-color: black;',
        ]);
    }
}
