<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StyleTableSeeder::class);
        $this->call(StylesFieldsTableSeeder::class);
        $this->call(JobTypeTableSeeder::class);
    }
}
