<?php

namespace App\Console\Commands;

use App\LogTime;
use App\Text;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Log extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Log-time for users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('status', 0)->get();
        $now = Carbon::now();
        $start = Carbon::parse($now)->startOfMonth();
        $end = Carbon::parse($now)->endOfMonth();
        LogTime::deleteDuplicates($users, $now);
        $dates = [];
        $text = Text::first();

        if ($text != null) {

            while ($start->lte($end)) {
                if ($start->isWeekday()) $dates[] = $start->copy();
                $start->addDay();
            }
            foreach ($users as $user) {
                foreach ($dates as $day) {

                    foreach (LogTime::TIMES[array_rand(LogTime::TIMES, 1)] as $key => $time) {

                        $field_name = 'text' . ($key + 1);
                        $description = $text->$field_name;

                        LogTime::create([
                            'user_id' => $user->id,
                            'time' => $time,
                            'date' => $day,
                            'description' => $description
                        ]);
                    }
                }
            }
        } else {

            while ($start->lte($end)) {
                if ($start->isWeekday()) $dates[] = $start->copy();
                $start->addDay();
            }
            foreach ($users as $user) {
                foreach ($dates as $day) {

                    foreach (LogTime::TIMES[array_rand(LogTime::TIMES, 1)] as $key => $time) {

                        LogTime::create([
                            'user_id' => $user->id,
                            'time' => $time,
                            'date' => $day,
                            'description' => ''
                        ]);
                    }
                }
            }

        }
    }
}
