<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{

    public $timestamps = false;
    protected $table = 'templates';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }

    public function style()
    {
        return $this->hasOne('App\Styles');
    }

    public static function createDefault()
    {
        $template = new self();

        $template->company_name = "Petar Petrović  PR Računarsko programiranje (ime agencije)";
        $informations =[[]];
        $informations[0] = ["feature" => "Adresa","value" => ""];
        $informations[1] = ["feature" => "Mesto","value" => ""];
        $informations[2] = ["feature" => "MB","value" => ""];
        $informations[3] = ["feature" => "PIB","value" => ""];
        $informations[4] = ["feature" => "Šifra delatnosti","value" => "6201"];
        $informations[5] = ["feature" => "Tekući račun","value" => ""];

        $template->informations = json_encode($informations);
        $template->styles_id = 1;
        $template->additional_text = htmlentities("(FIRMA) nije u sistemu PDV-a na osnovu čl.33 Zakona o PDV-u\r\n*Ova faktura je važeća bez potpisa i pečata");
        return $template;
    }
}
