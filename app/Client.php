<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');

    }
    public static function createDefault(){

        $client = new self();

        $client->company_name = "Quantox Technology d.o.o";

        $informations = [[]];

        $informations[0] = [ "feature" => "Adresa", "value" => "Kneza Vase Popovića 38"];
        $informations[1] = [ "feature" => "Mesto", "value" => "32000 Čačak"];
        $informations[2] = [ "feature" => "MB", "value" => "20154993"];
        $informations[3] = [ "feature" => "PIB", "value" => "104386102"];

        $client->informations = json_encode($informations);

        return $client;

    }
}
