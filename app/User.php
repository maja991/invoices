<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'confirmation_hash', 'job_id', 'project'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function templates()
    {
        return $this->hasMany('App\Template');
    }

    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    public function setTemplate(){
        $template = Template::createDefault();
        $template->user_id = $this->id;
        $template->save();
    }

    public function setClient(){
        $client = Client::createDefault();
        $client->user_id = $this->id;
        $client->save();
    }

    public function jobTypes() {

        return $this->belongsTo('App\JobType','job_id');
    }
}
