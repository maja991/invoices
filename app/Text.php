<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $table = 'texts';
    public $timestamps = false;

    protected $fillable = ['text1', 'text2', 'text3', 'text4'];
}
