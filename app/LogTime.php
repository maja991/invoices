<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LogTime extends Model
{
    protected $table = 'log_time';
    public $timestamps = false;

    protected $fillable = ['user_id', 'date', 'time', 'description'];

    const TIMES = [
        [30, 300, 75, 75], [30, 315, 60, 75], [30, 315, 75, 60], [30, 330, 45, 75], [30, 330, 60, 60],
        [30, 330, 75, 45], [45, 285, 75, 75], [45, 300, 60, 75], [45, 300, 75, 60], [45, 315, 45, 75],
        [45, 315, 60, 60], [45, 315, 75, 45], [45, 330, 45, 60], [45, 330, 60, 45], [60, 270, 75, 75],
        [60, 285, 60, 75], [60, 285, 75, 60], [60, 300, 45, 75], [60, 300, 60, 60], [60, 300, 75, 45],
        [60, 315, 45, 60], [60, 315, 60, 45], [60, 330, 45, 45], [75, 270, 60, 75], [75, 270, 75, 60],
        [75, 285, 45, 75], [75, 285, 60, 60], [75, 285, 75, 45], [75, 300, 45, 60], [75, 300, 60, 45],
        [75, 315, 45, 45], [90, 270, 45, 75], [90, 270, 60, 60], [90, 270, 75, 45], [90, 285, 45, 60],
        [90, 285, 60, 45], [90, 300, 45, 45]
    ];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public static function deleteDuplicates($users, $month)
    {
        $users_id = $users->pluck('id');
        $start = Carbon::parse($month)->startOfMonth();
        $end = Carbon::parse($month)->endOfMonth();

        $logs = LogTime::whereBetween('date', [$start, $end])
            ->whereIn('user_id', $users_id)
            ->get();

        foreach ($logs as $log) $log->delete();
        return;
    }
}
