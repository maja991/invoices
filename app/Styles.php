<?php
/**
 * Created by PhpStorm.
 * User: user10
 * Date: 26.12.16.
 * Time: 15.04
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Styles extends Model
{

    public $timestamps = false;
    protected $table = 'styles';

    public function templates()
    {
        return $this->belongsTo('App\Template');
    }

    public function styleFields()
    {
        return $this->hasMany('App\StylesFields');
    }
}