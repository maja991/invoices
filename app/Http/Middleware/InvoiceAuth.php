<?php

namespace App\Http\Middleware;

use App\Invoice;
use Closure;
use Illuminate\Support\Facades\Auth;

class InvoiceAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user())
            return redirect('/login');

        $invoiceID = $request->segment(2);

        if(!$invoiceID)
        {
            if($request->has('id'))
                $invoiceID = $request->id;
            else
                return redirect('/home');
        }

        $invoice =  Invoice::find($invoiceID);

        if ($invoice == null || $invoice->template->user->id !== Auth::user()->id) {
            return redirect('/invoice') -> withErrors(['msg'=> 'Nemate pravo na takvu akciju!']);
        }

        return $next($request);
    }
}
