<?php

namespace App\Http\Middleware;

use App\Template;
use Closure;
use Illuminate\Support\Facades\Auth;

class TemplateAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user())
            return redirect('/login');

        $templateID = $request->segment(2);

        if(!$templateID)
        {
            if($request->has('id'))
                $templateID = $request->id;
            else
                return redirect('/home');
        }

        $template =  Template::find($templateID);

        if ($template == null || $template->user->id!== Auth::user()->id) {
            return redirect('/template') -> withErrors(['msg'=> 'Nemate pravo na takvu akciju!']);
        }
        return $next($request);
    }
}
