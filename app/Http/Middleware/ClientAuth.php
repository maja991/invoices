<?php

namespace App\Http\Middleware;

use App\Client;
use Closure;
use Illuminate\Support\Facades\Auth;

class ClientAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user())
            return redirect('/login');

        $clientID = $request->segment(2);

        if(!$clientID)
        {
            if($request->has('id'))
                $clientID = $request->id;
            else
                return redirect('/home');
        }

        $client =  Client::find($clientID);

        if ($client == null || $client->user->id !== Auth::user()->id) {
            return redirect('/client') -> withErrors(['msg'=> 'Nemate pravo na takvu akciju!']);
        }
        return $next($request);
    }
}
