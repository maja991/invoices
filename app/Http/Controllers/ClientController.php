<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function index()
    {

        $user = Auth::user();
        $user_id = $user->id;
        $clients = $user->clients;
        $data = compact('user_id', 'clients');
        return view('client', $data);
    }

    public function store(Request $request)
    {
        $client = new Client();
        $client->user_id = Auth::user()->id;
        $client->company_name = $request->company_name;
        if ($request->informations[0]['feature'] == "" and $request->informations[0]['value'] == "") {
            $client->informations = null;
        } else {
            $client->informations = json_encode($request->informations);
        }
        $client->save();

        return $this->index();
    }


    public function destroy(Request $request)
    {

        $client = Client::find($request->id);
        $msg = "";
        if (isset($request->delete)) {
            try {
                $client->delete();
            } catch (QueryException $e) {
                $msg = "Ne možete obrisati odabranog klijenta dok postoje njegove fakture!";
            }
        }
        return redirect('/client')->withErrors(['msg' => $msg]);
    }

    public function edit($id)
    {

        $client = Client::find($id);
        $informations = json_decode($client->informations, true);
        $m = count($informations);

        $data = compact('client', 'informations', 'm');

        return view('clientEdit', $data);
    }

    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $client->company_name = $request->company_name;
        if ($request->informations[0]['feature'] == "" and $request->informations[0]['value'] == "") {
            $client->informations = null;
        } else {
            $client->informations = json_encode($request->informations);
        }
        $client->save();

        return $this->edit($id)->with('message', 'Promene uspešno sačuvane!! ');

    }

}
