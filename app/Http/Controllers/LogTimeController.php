<?php

namespace App\Http\Controllers;

use App\LogTime;
use App\Text;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class LogTimeController extends Controller
{
    public function index()
    {
        $users = User::where('status', 0)->get();

        return view('log.index', compact('users'));
    }

    public function show($id)
    {
        $user_id = User::find($id)->id;
        $logs = LogTime::where('user_id', $user_id)->get();

        return view('log.show', compact('logs'));
    }

    public function searchUser(Request $request)
    {
        $users = User::where('status', 0)
            ->where('name', 'like', '%' . $request->data_name . '%')
            ->get();

        return view('log.search', compact('users'));
    }

    public function searchMonth(Request $request)
    {
        $user_id = $request->user_id;

        $logs = LogTime::where('user_id', $user_id)
            ->whereMonth('date', $request->data)
            ->get();

        return view('log.month', compact('logs'));
    }

    public function downloadExcel(Request $request)
    {
        $user_id = $request->user_id;
        $month = $request->date;

        $logs = LogTime::where('user_id', $user_id)
            ->whereMonth('date', $month)
            ->get();

        $user = User::where('id', $user_id)->first();
        $name = $user->name;
        $year = date("Y");

        Excel::create($name . '_' . $month . '_' . $year, function ($excel) use ($logs) {
            $excel->sheet('ExportFile', function ($sheet) use ($logs) {
                $sheet->loadView('log.month')->with('logs', $logs)
                    ->setWidth(array(
                        'A' => 20,
                        'B' => 20,
                        'C' => 20,
                        'D' => 20,
                        'E' => 20
                    ))
                    ->setheight(array(
                        1 => 20,
                    ))
                    ->setStyle(array(
                        'font' => array(
                            'name' => 'Arial',
                            'size' => 12,
                        )
                    ))
                    ->setBorder('A1:E1')
                    ->cells('A1:E92', function ($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                    });
            });
        })->export('xls');
    }

    public function settings()
    {
        return view('log.settings');
    }

    public function logTime(Request $request)
    {
        $users = User::where('status', 0)->get();
        $month = $request->data;
        LogTime::deleteDuplicates($users, $month);
        $start = Carbon::parse($month)->startOfMonth();
        $end = Carbon::parse($month)->endOfMonth();
        $dates = [];
        $text = Text::first();

        if ($text != null) {

            while ($start->lte($end)) {
                if ($start->isWeekday()) $dates[] = $start->copy();
                $start->addDay();
            }
            foreach ($users as $user) {
                foreach ($dates as $day) {
                    foreach (LogTime::TIMES[array_rand(LogTime::TIMES, 1)] as $key => $time) {

                        $field_name = 'text' . ($key + 1);
                        $description = $text->$field_name;

                        LogTime::create([
                            'user_id' => $user->id,
                            'time' => $time,
                            'date' => $day,
                            'description' => $description
                        ]);
                    }
                }
            }
        } else {

            while ($start->lte($end)) {
                if ($start->isWeekday()) $dates[] = $start->copy();
                $start->addDay();
            }
            foreach ($users as $user) {
                foreach ($dates as $day) {
                    foreach (LogTime::TIMES[array_rand(LogTime::TIMES, 1)] as $key => $time) {

                        LogTime::create([
                            'user_id' => $user->id,
                            'time' => $time,
                            'date' => $day,
                            'description' => ''
                        ]);
                    }
                }
            }
        }
    }
}
