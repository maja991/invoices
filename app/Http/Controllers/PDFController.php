<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\StylesFields;
use Illuminate\Support\Facades\Auth;
use PDF;
use Illuminate\Http\Request;

class PDFController extends Controller
{

    public static function show($id)
    {
        $invoice         =  Invoice::find($id);
        $invoice->traffic_date    =  date('d.m.Y', strtotime($invoice->traffic_date));
        $invoice->publishing_date =  date('d.m.Y', strtotime($invoice->publishing_date));
        $invoice->currency_date   =  date('d.m.Y', strtotime($invoice->currency_date));
        $template        =  $invoice->template;
        $client          =  $invoice->client;
        $items           =  json_decode($invoice->items, true);
        $informationsTem =  json_decode($template->informations, true);
        $informationsCli =  json_decode($client->informations, true);
        $m               =  count($informationsCli);
        $n               =  count($informationsTem);

        $tablePadding    =  StylesFields::where('style_id', $template->styles_id)
            ->where('properties', 'tablePadding')
            ->first();
        if (isset($tablePadding)){
            $tablePadding = $tablePadding->value;
        } else {
            $tablePadding = "";
        }

        $tableBorder     =  StylesFields::where('style_id', $template->styles_id)
            ->where('properties', 'tableBorder')
            ->first();
        if (isset($tableBorder)){
            $tableBorder = $tableBorder->value;
        } else {
            $tableBorder = "";
        }

        $data  = [
            'invoice'  => $invoice,
            'template' => $template,
            'client'   => $client,
            'items'    => $items ,
            'informationsCli' => $informationsCli,
            'm'  =>  $m,
            'informationsTem' => $informationsTem,
            'n' => $n,
            'tablePadding' => $tablePadding,
            'tableBorder' => $tableBorder
        ];

        $pdf = PDF::loadView('pdf',$data);
        return $pdf->stream('');
    }


}
