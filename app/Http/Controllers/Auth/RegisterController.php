<?php

namespace App\Http\Controllers\Auth;

use App\JobType;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users|email_domain:' . $data['email'],
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $hash = str_random(64);

        $mailData = [
            'introLines' => ['Please verify your email address.'],
            'actionText' => 'Confirm',
            'actionUrl' => config('app.url') . '/register/verify/' . $hash,
            'level' => 'success',
            'outroLines' => ['']
        ];

        Mail::send('vendor.notifications.email', $mailData, function ($m) use ($data) {
            $m->from('no-reply@quantox.com', 'Quantox Pausal');

            $m->to($data['email'], $data['name'])->subject('Registration from Quantox service');
        });

        $jobType = JobType::where('name', 'undefined')->first();

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_hash' => $hash,
            'job_id' => $jobType->id,
            'project' => ''
        ]);
    }

    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function confirm($hash)
    {
        $user = User::where('confirmation_hash', $hash)->where('active', 0)->first();

        if (!$user || $hash == '') {
            abort(404, "Unknown user");
        } else {
            $user->active = 1;
            $user->save();
            return redirect('/login');
        }
    }

    protected function registered(Request $request, $user)
    {
        $user->setTemplate();
        $user->setClient();
    }
}
