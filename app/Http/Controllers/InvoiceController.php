<?php

namespace App\Http\Controllers;


use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    public function index()
    {
        $user          =   Auth::user();
        $user_id       =   $user->id;
        $clients       =   $user->clients;
        $templates     =   $user->templates;
        $invoicesArray =   [];


        foreach ( $templates as $template ) {
            foreach ( $template->invoices as $invoice ) {
                $invoicesArray[] = $invoice;
            }
        }
        $invoiceNumber          =   $user->invoice_num+1;
        $year                   =   $user->year;
        $traffic_date           =   date('Y-m-t', strtotime("-1 month"));
        $publishing_date        =   date('Y-m-t', strtotime("-1 month"));
        $currency_date          =   date ('Y-m-15', $_SERVER['REQUEST_TIME']);
        $location               =   $user->place;
        $item                   =   json_decode($user->item, true);


        $data  =  [
            'clients'  => $clients,
            'templates' => $templates,
            'invoiceNumber' =>  $invoiceNumber,
            'year'  =>  $year,
            'traffic_date'  =>  $traffic_date,
            'publishing_date'  => $publishing_date,
            'currency_date' => $currency_date,
            'user_id'  => $user_id,
            'invoices'  => $invoicesArray,
            'location' => $location,
            'item' => $item
        ];


        return view('invoice')->with($data);
    }

    public function store(Request $request)
    {

        $invoice                  =  new Invoice();
        $invoice->template_id     =  $request->template;
        $invoice->client_id       =  $request->client;
        $invoice->location        =  $request->location;
        $invoice->traffic_date    =  $request->traffic_date;
        $invoice->publishing_date =  $request->publishing_date;
        $invoice->currency_date   =  $request->currency_date;
        $invoice->invoice_num     =  $request->invoice_num;
        $invoice->year            =  $request->year;
        $amount                   =  0;
        $items = $request->items;
        foreach ($items as $item){
            $amount              +=  $item['price'];
        }
        $invoice->amount          = $amount;
        $invoice->items           = json_encode($request->items);

        $user                     = Auth::user();
        $user->invoice_num        = $request->invoice_num;
        $user->year               = $request->year;
        $user->place              = $request->location;
        $user->item               = json_encode($request->items[0]);
        $user->save();
        $invoice->save();

        return $this->index();
    }



    public function destroy(Request $request){

        $invoice = Invoice::find($request->id);
        if (isset($request->delete)) {
            $invoice->delete();
        }
        return $this->index();
    }

    public function edit ($id)
    {
        $invoice         =  Invoice::find($id);
        $items           =  json_decode($invoice->items, true);
        $user            =  Auth::user();
        $clients         =  $user->clients;
        $templates       =  $user->templates;
        $m               =  count($items);
        $data            =  compact('invoice','items', 'clients','templates','m');

        return view('invoiceEdit', $data);
    }

    public function update(Request $request, $id)
    {
        $invoice                  =  Invoice::find($id);

        $invoice->template_id     =  $request->template;
        $invoice->client_id       =  $request->client;
        $invoice->location        =  $request->location;
        $invoice->traffic_date    =  $request->traffic_date;
        $invoice->publishing_date =  $request->publishing_date;
        $invoice->currency_date   =  $request->currency_date;
        $invoice->invoice_num     =  $request->invoice_num;
        $invoice->year            =  $request->year;
        $amount                   =  0;
        $items                    = $request->items;
        foreach ($items as $item){
            $amount              +=  $item['price'];
        }
        $invoice->amount          = $amount;
        $invoice->items           = json_encode($request->items);

        $user                     = Auth::user();
        $user->invoice_num        = $request->invoice_num;
        $user->year               = $request->year;
        $user->save();
        $invoice->save();

        return $this->edit($id)->with('message', 'Promene uspešno sačuvane!! ');

    }
}
