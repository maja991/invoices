<?php

namespace App\Http\Controllers;

use App\Text;
use Illuminate\Http\Request;

class TextController extends Controller
{
    public function edit()
    {
        $text = Text::all()->first();

        return view('text.edit', compact('text'));
    }

    public function update(Request $request)
    {
        $text = Text::first();

        if ($text != null) {
            $text->text1 = $request->text1;
            $text->text2 = $request->text2;
            $text->text3 = $request->text3;
            $text->text4 = $request->text4;
            $text->update();

        } else {
            $text = Text::create($request->all());
            $text->save();
        }
        return redirect()->route('text')
            ->with('success', 'Tekstovi su uspesno izmenjeni.');
    }
}
