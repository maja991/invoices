<?php

namespace App\Http\Controllers;

use App\Styles;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Template;
use Illuminate\Support\Facades\Auth;

class TemplateController extends Controller
{
    public function index(){

        $user            =   Auth::user();
        $user_id         =   $user->id;
        $templates       =   $user->templates;
        $styles          =   Styles::all();
        $data            =   compact('user_id', 'templates','styles');

        return view('template', $data);
    }


    public function store(Request $request)
    {

        $template                  = new Template();

        $template->user_id         = Auth::user()->id;
        $template->company_name    = $request->company_name;
        if ($request->informations[0]['feature']=="" and $request->informations[0]['value']==""){
            $template->informations=null;
        } else {
            $template->informations = json_encode($request->informations);
        }
        $template->styles_id       = $request->styles_id;
        $template->additional_text = htmlentities($request->additional_text);
        $template->save();

        return $this->index();
    }

    public function destroy(Request $request){

        $template = Template::find($request->id);
        $msg="";
        if (isset($request->delete)) {
            try {
                $template->delete();
            }
            catch ( QueryException $e){
                $msg="Ne možete obrisati izabrani templejt, jer se on koristi na nekoj od Vaših faktura!";
            }
        }
        return redirect('/template') -> withErrors(['msg'=> $msg]);
    }

    public function edit ($id)
    {

        $template        =  Template::find($id);
        $styles          =  Styles::all();
        $informations    =  json_decode($template->informations, true);
        $n               =  count($informations);

        $data            =  compact('template','informations','n', 'styles');

        return view('templateEdit', $data);
    }

    public function update(Request $request, $id)
    {

        $template = Template::find($id);

        $template->company_name       = $request->company_name;
        if ($request->informations[0]['feature']=="" and $request->informations[0]['value']==""){
            $template->informations=null;
        } else {
            $template->informations   = json_encode($request->informations);
        }
        $template->styles_id          = $request->styles_id;
        //$template->additional_text    = htmlentities($request->additional_text);

        $template->save();

        return $this->edit($id)->with('message', 'Promene uspešno sačuvane!!');
    }
}
