<?php

namespace App\Http\Controllers;

use App\Client;
use App\Invoice;
use App\JobType;
use App\LogTime;
use App\Template;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        $users = User::where('status', 0)->get();

        return view('users.index', ['users' => $users]);
    }

    public function create()
    {
        $jobs = JobType::all();

        return view('users.create', compact('jobs'));
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|max:255|regex:/(.*)@quantox\.com/i|unique:users',
            'job_id' => 'required',
            'project' => 'required'
        ];
        $messages = [
            'name.required' => 'Obavezno polje :attribute.',
            'email.required' => 'Obavezno polje :attribute.',
            'email' => 'Email mora biti validan.',
            'email.regex' => 'Email domen mora biti "quantox.com".',
            'unique' => 'Email je vec u upotrebi.',
            'job_id.required' => 'Obavezno polje :attribute.',
            'project.required' => 'Obavezno polje :attribute.'
        ];
        $customAttributes = [
            'name' => 'ime',
            'job_id' => 'tip posla',
            'project' => 'projekat'
        ];
        $this->validate(request(), $rules, $messages, $customAttributes);

        $hash = str_random(64);
        $password = str_random(8);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'confirmation_hash' => $hash,
            'job_id' => $request->job_id,
            'project' => $request->project,
        ]);
        $user->active = 1;
        $user->setTemplate();
        $user->setClient();
        $user->save();

        return redirect()->route('users.index')
            ->with('success', 'Korisnik je uspesno kreiran.');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $jobs = JobType::all();

        return view('users.edit', compact('user', 'jobs'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'job_id' => 'required',
            'project' => 'required'
        ];
        if ($user->email != $request->email) {
            $rules['email'] = 'required|email|max:255|regex:/(.*)@quantox\.com/i|unique:users';
        }
        $messages = [
            'name.required' => 'Obavezno polje :attribute.',
            'email.required' => 'Obavezno polje :attribute.',
            'email' => 'Email mora biti validan.',
            'email.regex' => 'Email domen mora biti "quantox.com".',
            'unique' => 'Email je vec u upotrebi.',
            'job_id.required' => 'Obavezno polje :attribute.',
            'project.required' => 'Obavezno polje :attribute.'
        ];
        $customAttributes = [
            'name' => 'ime',
            'job_id' => 'tip posla',
            'project' => 'projekat'
        ];
        $this->validate(request(), $rules, $messages, $customAttributes);

        $user->update($request->all());

        return redirect()->route('users.index')
            ->with('success', 'Korisnik je uspesno izmenjen.');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        LogTime::where('user_id', $id)->delete();
        $template_id = Template::where('user_id', $id)->first();
        $client_id = Client::where('user_id', $id)->first();

        Invoice::whereIn('template_id', $template_id)
            ->whereIn('client_id', $client_id)
            ->delete();

        Template::where('user_id', $id)->delete();
        Client::where('user_id', $id)->delete();
        $user->delete();

        return redirect()->route('users.index')
            ->with('success', 'Korisnik je uspesno izbrisan.');
    }
}
