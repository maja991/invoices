<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Invoice;
use Illuminate\Support\Facades\Auth;
use App\Template;
use App\Client;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index()
    {

        $user          =  Auth::user();
        $clients       =  $user->clients;
        $templates     =  $user->templates;

        $invoicesArray = [];
        foreach ( $templates as $template ) {
            foreach ( $template->invoices as $invoice ) {
                $invoicesArray[] = $invoice;
            }
        }
        $clientInfo    = [];
        foreach ($clients as $client){
            $clientInfo[$client->id] = json_decode($client->informations, true);
        }

        $templateInfo  = [];
        foreach ($templates as $template){
            $templateInfo[$template->id] = json_decode($template->informations, true);
        }

        $data =  [
            'clients'=>$clients,
            'templates'=>$templates,
            'invoices'=>$invoicesArray,
             'clientInfo' =>$clientInfo,
            'templateInfo' =>$templateInfo
        ];

        return view('index')->with($data);
    }

    public function show()
    {
        $users = null;
        if (Input::has('date_from') && Input::has('date_to'))
        {
            $from = date_create(Input::get('date_from'));
            $to = date_create(Input::get('date_to'));
            $users = User::where('active', 1)
                     ->where('status', 0) ->get();
            foreach ($users as $user){
                $templates = $user->templates;
                $user->invoices = Invoice::whereIn('template_id', $templates->pluck('id'))
                                  ->whereBetween('created_at', [$from,$to])->get();
            }

        }
        return view('supervise', ['users' => $users] );

    }

}
