<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    public function template()
    {
        return $this->belongsTo('App\Template');
    }

    public function client(){
        return $this->belongsTo('App\Client');
    }

}
