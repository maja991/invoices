<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
    protected $table='job_types';
    public $timestamps = false;

    protected $fillable=['name'];
}
