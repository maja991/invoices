<?php
/**
 * Created by PhpStorm.
 * User: user10
 * Date: 26.12.16.
 * Time: 15.05
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class StylesFields extends Model
{
    public $timestamps = false;
    protected $table = 'style_fields';

    public function style()
    {
        return $this->belongsTo('App\Styles');
    }

}