$(document).ready(function(){


  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });
  
});


$(document).ready(function() {
    var max_fields      = 20;                    //maximum input boxes allowed
    var wrapper         = $(".wrap");            //Fields wrapper
    var add_button      = $(".addButton");       //Add button ID

    $(add_button).click(function(e){             //on add input button click
        e.preventDefault();
        if(x < max_fields){                       //max input box allowed
            x++;                                  //item box increment
            $(wrapper).append("<div class='row'><div class='form-group' id='"+ (x-1) +"'>" +
                "<label class='col-xs-1 control-label'></label>" +
                "<div class='col-xs-7'> " +
                "<textarea class='form-control' name='items["+ (x-1) +"][description]' required placeholder='Opis'></textarea>" +
                "</div><div class='col-xs-3'> " +
                "<input type='number' class='form-control' name='items["+ (x-1) +"][price]' required placeholder='Cena'/></div> " +
                "<div class='col-xs-1'>" +
                "<button type='button' class='btn btn-default remove_field'><i class='fa fa-minus'></i></button> " +
                "</div>" +
                "</div></div>") ; //add input box

        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault();
        for(var i= $(this).parent('div').parent('div').attr("id");i<=x;i++)
        {
            $('textarea[name="items\['+i+'\]\[description\]"]').attr("name","items\["+ (i-1) +"\]\[description\]");
            $('input[name="items\['+i+'\]\[price\]"]').attr("name","items\["+ (i-1) +"\]\[price\]");
            $('#'+i).attr("id", i-1 );

        }
        $(this).parent('div').parent('div').parent('div').remove();
        x--;

    })
});


$(document).ready(function(){
    $(".client").click(function(){
        var client = $(this).data('name');
        var info = $(this).data('info');
        $(".appended").remove();
        $("#company_name_cli").append("<b class='appended'>"+ client + "</b>");
        if (info != "") {
            $.each(info, function (index, value) {
                $("#clientTable").append("<tr class='appended'><th>"+ value.feature+"</th><td>"+ value.value+"</td></tr>");
            });
        } else {
            $("#clientTable").append("<thead class='appended'><tr><td>Nema informacija</td></tr></thead>");
        };
        $("#clientModal").modal();
    });
});

var errmsg  =  "Popunite ovo polje";

$(document).ready(function() {

    var max_fieldsCli      = 7;                        //maximum input boxes allowed
    var wrapperCli         = $(".wrapClient");         //Fields wrapper
    var add_buttonCli      = $(".addButton1");         //Add button ID


    $(add_buttonCli).click(function(e){                 //on add input button click
        e.preventDefault();
        if(x < max_fieldsCli){                          //max input box allowed
            x++;                                        //item box increment
            $(wrapperCli).append("<div class='row'><div class='form-group' id='"+ (x-1) +"'>" +
                "<label class='col-xs-3 control-label'></label>" +
                "<div class='col-xs-4'> " +
                "<input type='text'class='form-control' required name='informations[" + (x-1) + "][feature]'/>" +
                "</div><div class='col-xs-4'> " +
                "<input type='text' class='form-control' required name='informations[" + (x-1) + "][value]'/> " +
                "</div>" +
                "<div class='col-xs-1'>" +
                "<button type='button' class='btn btn-default remove_field1'><i class='fa fa-minus'></i></button> " +
                "</div></div>") ; //add input box

        }
    });

    $(wrapperCli).on("click",".remove_field1", function(e){ //user click on remove text
        e.preventDefault();
        for(var i= $(this).parent('div').parent('div').attr("id");i<=x;i++)
        {
            $('input[name="informations\['+i+'\]\[feature\]"]').attr("name","informations\["+ (i-1) +"\]\[feature\]");
            $('input[name="informations\['+i+'\]\[value\]"]').attr("name","informations\["+ (i-1) +"\]\[value\]");
            $('#'+i).attr("id", i-1 );

        }
        $(this).parent('div').parent('div').parent('div').remove();
        var $this = $(this).parent('div').parent('div').parent('div');
        console.log($this);
        x--;

    });
});

$(document).ready(function() {

    var max_fieldsTem      = 7;                    	    //maximum input boxes allowed
    var wrapperTem         = $(".wrapTemplate");        //Fields wrapper
    var add_buttonTem      = $(".addButtonTem");        //Add button ID


    $(add_buttonTem).click(function(e){                 //on add input button click
        e.preventDefault();
        if(x < max_fieldsTem){                          //max input box allowed
            x++;                                        //item box increment
            $(wrapperTem).append("<div class='row'><div class='form-group' id='"+ (x-1) +"'>" +
                "<label class='col-xs-3 control-label'></label>" +
                "<div class='col-xs-4'> " +
                "<input type='text'class='form-control' required name='informations[" + (x-1) + "][feature]'/>" +
                "</div><div class='col-xs-4'>" +
                "<input type='text' class='form-control' required name='informations[" + (x-1) + "][value]'/>" +
                "</div>" +
                "<div class='col-xs-1'>" +
                "<button type='button' class='btn btn-default remove_field1'><i class='fa fa-minus'></i></button>" +
                "</div></div>") ; //add input box

        }
    });

    $(wrapperTem).on("click",".remove_field1", function(e){ //user click on remove text
        e.preventDefault();
        for(var i= $(this).parent('div').parent('div').attr("id");i<=x;i++)
        {
            $('input[name="informations\['+i+'\]\[feature\]"]').attr("name","informations\["+ (i-1) +"\]\[feature\]");
            $('input[name="informations\['+i+'\]\[value\]"]').attr("name","informations\["+ (i-1) +"\]\[value\]");
            $('#'+i).attr("id", i-1 );

        }
        $(this).parent('div').parent('div').parent('div').remove();
        x--;

    });
});