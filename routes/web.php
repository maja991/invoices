<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegisterController@confirm'
]);

Auth::routes();

Route::group(['middleware' => 'RegularUser'], function () {

    Route::get('/home', 'HomeController@index');
    Route::get('/client', 'ClientController@index');
    Route::post('/client', 'ClientController@store');
    Route::get('/template', 'TemplateController@index');
    Route::post('/template', 'TemplateController@store');
    Route::get('/invoice', 'InvoiceController@index');
    Route::post('/invoice', 'InvoiceController@store');
});

Route::group(['middleware' => 'InvoiceAuth'], function () {

    Route::delete('/invoice', 'InvoiceController@destroy');
    Route::get('/invoice/{id}/edit', 'InvoiceController@edit');
    Route::put('/invoice/{id}/edit', 'InvoiceController@update');
});

Route::get('/pdf/{id}', 'PDFController@show')->middleware('PDF_preview');

Route::group(['middleware' => 'ClientAuth'], function () {

    Route::get('/client/{id}/edit', 'ClientController@edit');
    Route::put('/client/{id}/edit', 'ClientController@update');
    Route::delete('/client', 'ClientController@destroy');

});

Route::group(['middleware' => 'TemplateAuth'], function () {

    Route::get('/template/{id}/edit', 'TemplateController@edit');
    Route::put('/template/{id}/edit', 'TemplateController@update');
    Route::delete('/template', 'TemplateController@destroy');

});

Route::get('/supervise', 'HomeController@show')->middleware('OnlySupervisor');

Route::group(['middleware' => 'OnlySupervisor'], function () {
    Route::resource('users', 'UserController');
});

Route::group(['middleware' => 'OnlySupervisor'], function () {
    Route::resource('log', 'LogTimeController');
});

Route::get('/search', 'LogTimeController@searchUser')->middleware('OnlySupervisor');

Route::get('/month', 'LogTimeController@searchMonth')->middleware('OnlySupervisor');

Route::get('downloadExcel', 'LogTimeController@downloadExcel')->middleware('OnlySupervisor');;

Route::get('/text', 'TextController@edit')->name('text')->middleware('OnlySupervisor');

Route::get('update', 'TextController@update')->middleware('OnlySupervisor');

Route::get('/settings', 'LogTimeController@settings')->middleware('OnlySupervisor');

Route::get('/logs', 'LogTimeController@logTime')->middleware('OnlySupervisor');
